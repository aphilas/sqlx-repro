package main

import (
	"database/sql/driver"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"time"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"

	_ "github.com/jackc/pgx/v4/stdlib"
)

type FeatureRequest struct {
	ID              uuid.UUID       `db:"id"`
	Title           string          `db:"title"`
	UserID          uuid.NullUUID   `db:"user_id"`
	CreatedAt       time.Time       `db:"created_at"`
	UpdatedAt       time.Time       `db:"updated_at"`
	ClientIPAddress string          `db:"client_ip_address"`
	FeatureMetaData FeatureMetaData `db:"feature_meta_data" json:"feature_meta_data"`
}

type FeatureMetaData struct {
	FeatureProductArea FeatureProductArea `db:"feature_product_area" json:"feature_product_area"`
}

// Implement the sql.Scanner interface for FeatureMetaData
func (fmd *FeatureMetaData) Scan(value interface{}) error {
	if value == nil {
		return nil
	}

	bytes, ok := value.([]byte)
	if !ok {
		return errors.New("type assertion .([]byte) failed")
	}

	var fmdFromDB FeatureMetaData
	if err := json.Unmarshal(bytes, &fmdFromDB); err != nil {
		return err
	}

	*fmd = fmdFromDB
	return nil
}

// Implement the driver.Valuer interface to write FeatureMetaData back to the database
func (fmd FeatureMetaData) Value() (driver.Value, error) {
	return json.Marshal(fmd)
}

type FeatureProductArea string

const (
	Planners     FeatureProductArea = "planners"
	QuickPlans   FeatureProductArea = "qp"
	Transactions FeatureProductArea = "transactions"
	Chat         FeatureProductArea = "chat"
)

type PostgresRepository struct {
	connection *sqlx.DB
}

const (
	ddlSql = `
	CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

	DROP TABLE IF EXISTS feature_requests;

	CREATE TABLE feature_requests(
		id UUID NOT NULL DEFAULT uuid_generate_v4(),
		title VARCHAR(255) NOT NULL,
		user_id UUID NOT NULL,
		created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
		updated_at TIMESTAMP NULL,
		feature_meta_data JSONB NULL,
		PRIMARY KEY (id)
	);
	`
)

func main() {
	pr, err := NewPostgresRepository()
	if err != nil {
		log.Fatal(err)
	}

	pr.connection.MustExec(ddlSql)

	userID := uuid.New()

	u := FeatureRequest{
		ID:    uuid.New(),
		Title: "title",
		UserID: uuid.NullUUID{
			Valid: true,
			UUID:  userID,
		},
		CreatedAt: time.Now().UTC(),
		FeatureMetaData: FeatureMetaData{
			FeatureProductArea: Chat,
		},
	}

	_, err = pr.connection.Exec(
		`
		INSERT INTO feature_requests 
			(id, title, feature_meta_data, user_id, created_at, updated_at)
		VALUES 
			($1, $2, $3, $4, $5, $6)
		`,
		u.ID, u.Title, u.FeatureMetaData, u.UserID, u.CreatedAt, u.UpdatedAt,
	)
	if err != nil {
		log.Fatal(err)
	}

	_, err = pr.GetFeatureRequestByUserID(userID)
	if err != nil {
		log.Fatal(err)
	}
}

func NewPostgresRepository() (*PostgresRepository, error) {
	url := fmt.Sprintf(
		"host=%s port=%s user=%s password=%s dbname=%s sslmode=%s",
		"localhost",
		"5433",
		"dev",
		"djZMi4hGgSLpbc1B",
		"cashflow",
		"disable",
	)

	db, err := sqlx.Connect("pgx", url)
	if err != nil {
		return nil, fmt.Errorf("connect database: %w", err)
	}

	return &PostgresRepository{db}, nil
}

func (pr *PostgresRepository) GetFeatureRequestByUserID(userID uuid.UUID) ([]FeatureRequest, error) {
	featureRequests := []FeatureRequest{}

	query := `SELECT id, title, feature_meta_data, user_id, created_at, updated_at FROM feature_requests WHERE user_id = $1`

	err := pr.connection.Select(&featureRequests, query, userID)
	if err != nil {
		return nil, err

	}
	return featureRequests, nil
}
